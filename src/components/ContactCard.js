import React from 'react'


const ContactCard = (props) => {
     const {id,name,email} = props.contact;
    return (
        <div className='mx-3'>
            <div className='w-full px-3 py-3 my-1 bg-yellow-300 flex items-center	 flex-row rounded-xl hover:rounded-t-2xl hover:bg-yellow-400 cursor-pointer'>
                <div className="rounded-full w-10 h-10 bg-yellow-500 "></div>
                <div className=' flex flex-col flex-grow pl-3'>
                    <div className='text-lg font-bold'>{name}</div>
                    <div className='text-lg'> {email}</div>
                </div>
                <div className="flex flex-col">
                    <div className='cursor-pointer'>Delete</div>
                    <div className='cursor-pointer'>Edit</div>
                </div>
            </div>
        </div>
    )
}
export default ContactCard
