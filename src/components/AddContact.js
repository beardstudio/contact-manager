import React from 'react'

class AddContact extends React.Component{
    state ={
        name:"",
        email:"",
    }
    add = (e) =>{
        e.preventDefault();
        if(this.state.name === "" || this.state.email === ""){
            alert ("Oops ! Fields are empty");
            return
        }
        this.props.addContactHandler(this.state);
        this.setState({name:"",email:""});
        

    }
    render(){
        return(
            <div className="flex flex-col w-full px-3">
                <form action="" onSubmit={this.add}>
                <h2 className="text-lg text-center">Add Contact</h2>
                
                <div className="flex flex-col ">
                    <label htmlFor="name" className='text-lg font-semibold text-amber-800'>Name</label>
                    <input type="text" 
                    onChange={(e) => this.setState({name:e.target.value})} 
                    value={this.state.name} 
                    name="name" id="namex" placeholder='Enter name' 
                     className='h-10 pl-2 bg-amber-100'/>
                </div>
                <div className="flex flex-col mt-5">
                    <label htmlFor="email" className='text-lg font-semibold text-amber-800'>Email</label>
                    <input type="text" 
                    onChange={(e) => this.setState({email:e.target.value})} 
                    value={this.state.email}
                     name="email" id="namex" placeholder='Enter Email'  className='h-10 pl-2 bg-amber-100'/>
                </div>
                <button className='py-3 w-full mt-5 text-white rounded bg-amber-400 hover:bg-amber-500 hover:rounded-lg '>Add</button>
                </form>
                
            </div>
        )
    }
}

export default AddContact
